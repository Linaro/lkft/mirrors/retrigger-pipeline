#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2024-present Linaro Limited
#
# SPDX-License-Identifier: MIT

import json
import logging
import os
from pathlib import Path
import subprocess as sp

import requests

#
#   Set up logging
#
logger = logging.getLogger()
logger.setLevel(
    os.getenv("DEBUG")
    and logging.DEBUG
    or getattr(logging, os.getenv("LOGLEVEL", "INFO"))
)
ch = logging.StreamHandler()
ch.setFormatter(logging.Formatter("[%(levelname)s] %(message)s"))
logger.addHandler(ch)


#
#   Read entire env into settings
#
class Settings:
    def __init__(self, env=os.environ, extra=[]):
        # Set up the default environment from base.yml if not in pipeline
        if not os.getenv("CI") == "true":
            import yaml

            with open("base.yml") as default_env:
                dict_env = yaml.safe_load(default_env)
                for key, val in dict_env["variables"].items():
                    if os.environ.get(key) is None:
                        os.environ[key] = str(val)

        self.env = env

        if not self.validate(extra):
            return

        longest_string = 0
        for k, v in env.items():
            if len(k) > longest_string:
                longest_string = len(k)
            setattr(self, k, v)

        if self.DEBUG:
            longest_string += 1
            for k, v in env.items():
                print(f"{k.ljust(longest_string)}{v}")

    def validate(self, extra_vars=[]):
        """
        Run a minimum requirement check on env vars or other
        aspects of the environment that need to be present
        """
        required_vars = []
        # only check for Gitlab CI vars if we are running in a pipeline environment
        if os.getenv("CI") == "true":
            required_vars = [
                # Pre-defined variables coming from Gitlab-CI
                # Gitlab's APIv4 base URL
                "CI_API_V4_URL",
                # true if the job is running for a protected reference (branch or tag), empty otherwise
                "CI_COMMIT_REF_PROTECTED",
                # The first eight characters of the commit that triggered the pipeline
                "CI_COMMIT_SHORT_SHA",
                # Job's unique ID
                "CI_JOB_ID",
                # Job's name which is defined in each ci script, e.g.: build-meta-ts-qemuarm64-secureboot
                "CI_JOB_NAME",
                # Pipeline's ID
                "CI_PIPELINE_ID",
                # Event that triggered the pipeline
                # Can be push, web, schedule, api, external, chat, webide, merge_request_event, external_pull_request_event, parent_pipeline, trigger, or pipeline
                "CI_PIPELINE_SOURCE",
                # Full path of the cloned repository in the runner file system
                "CI_PROJECT_DIR",
                # Gitlab's unique project ID
                "CI_PROJECT_ID",
                # The project namespace with the project name included
                "CI_PROJECT_PATH",
                # The project URL, e.g. https://gitlab.com/Linaro/blueprints/ci
                "CI_PROJECT_URL",
                # Last part of project path
                "CI_PROJECT_NAME",
                # The address of the GitLab Container Registry: registry.gitlab.com
                "CI_REGISTRY",
                # The address of the Gitlab server: gitlab.com
                "CI_SERVER_HOST",
            ]
        required_vars += extra_vars

        self.missing = [v for v in required_vars if getattr(self, v) is None]
        if len(self.missing):
            logger.warning(
                f"The following environment variables are missing: {self.missing}"
            )
            return False

        for v in required_vars:
            logger.info(f"{v}={os.getenv(v)}")

        log_squad_link_from_testrun(self)

        return True

    def __getattr__(self, name):
        return self.env.get(name)

    def get_artifacts(self):
        artifact_list = self.ARTIFACTS.split(",") if self.ARTIFACTS else []
        return dict((item.split(":") for item in artifact_list))


def run_cmd(cmd):
    """
    Run the specified `cmd` and waits for its completion.
    Returns `proc` with extra attributes:
    - `ok` True if the the command returned 0, False otherwise
    - `out` decoded command stdout
    - `err` decoded command stderr
    """
    logger.info(f"Running {cmd}")
    proc = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.PIPE)
    stdout, stderr = proc.communicate()
    proc.ok = proc.returncode == 0

    proc.out = stdout.decode()
    proc.err = stderr.decode()
    return proc


#
#   Download any file from URL and save it to output_filename
#
def download_file(url, output_filename=None):
    output_filename = Path(output_filename or os.path.basename(url))
    try:
        MB = 1024**2
        download = requests.get(url, stream=True)
        show_progress = logger.isEnabledFor(logging.INFO)
        with output_filename.open("wb") as f:
            for chunk in download.iter_content(chunk_size=MB):
                f.write(chunk)
                if show_progress:
                    print(".", end="", flush=True)

        if show_progress:
            print(" OK")
    except requests.exceptions.HTTPError as e:
        logger.warning(f"Could not download {url}: {e}")
        return False
    return True


#
# Log the settings to a json so results can be later looked up
#
def log_settings(settings, required_vars, log_file="settings.json"):
    settings_dict = {}
    for required_var in required_vars:
        settings_dict[required_var] = getattr(settings, required_var)
    with open(log_file, "w") as f:
        json.dump(settings_dict, f)


#
# Log output of process - default to only logging on error
#
def log_proc(proc, only_on_error=True):
    if not proc.ok or not only_on_error:
        logger.info(proc.out)
    if not proc.ok:
        logger.error(proc.err)
    elif not only_on_error:
        logger.info(proc.err)


#
# Install prerequisites
#
def install_prerequisites(settings):
    """
    Install prerequisites
    """
    pip_install = run_cmd(["pip", "install", "-r", "requirements.txt"])
    log_proc(pip_install)
    if not pip_install:
        return False
    pip_upgrade_tuxsuite = run_cmd(["pip", "install", "--upgrade", "tuxsuite"])
    log_proc(pip_upgrade_tuxsuite)

    if not pip_upgrade_tuxsuite.ok:
        return False

    return True


#
# Log the URL of the original test/build's TestRun from SQUAD
#
def log_squad_link_from_testrun(settings):
    testrun_url_string = f"{settings.QA_SERVER}/api/testruns/"
    if settings.BUILD_TESTRUN_ID:
        logger.info(
            f"Original build TestRun ID to retrigger {settings.BUILD_TESTRUN_ID}: {testrun_url_string}{settings.BUILD_TESTRUN_ID}"
        )
    if settings.TEST_TESTRUN_ID:
        logger.info(
            f"Original test TestRun ID to retrigger {settings.TEST_TESTRUN_ID}: {testrun_url_string}{settings.TEST_TESTRUN_ID}"
        )


#
# Checks for each stage in retrigger process
#


def check_download_prerequisites(settings):
    # Currently no specific requirements for this stage
    return True


def check_fetch_reproducer(settings):
    if not settings.BUILD_TESTRUN_ID and not settings.TEST_TESTRUN_ID:
        logger.error(
            "No build or test TestRun ID provided - please provide at least one TestRun ID via TEST_TESTRUN_ID or BUILD_TESTRUN_ID."
        )
        return False

    if settings.TEST_TESTRUN_ID:
        if not settings.TEST_TESTRUN_ID.isdigit():
            logger.error(
                f"TEST_TESTRUN_ID provided but is not int {settings.TEST_TESTRUN_ID}"
            )
            return False

    if settings.BUILD_TESTRUN_ID:
        if not settings.BUILD_TESTRUN_ID.isdigit():
            logger.error(
                f"BUILD_TESTRUN_ID provided but is not int {settings.BUILD_TESTRUN_ID}"
            )
            return False

    return True


def check_create_plan(settings):
    if not settings.TEST_TESTRUN_ID and not settings.BUILD_TESTRUN_ID:
        logger.error(
            "TEST_TESTRUN_ID or BUILD_TESTRUN_ID must be provided to indicate whether a test or build reproducer was fetched."
        )
        return False
    if settings.TEST_TESTRUN_ID and not settings.TEST_REPRODUCER_FILE:
        logger.error(
            "TEST_TESTRUN_ID was provided but no name for TEST_REPRODUCER_FILE was provided"
        )
        return False
    if settings.BUILD_TESTRUN_ID and not settings.BUILD_REPRODUCER_FILE:
        logger.error(
            "BUILD_TESTRUN_ID was provided but no name for BUILD_REPRODUCER_FILE was provided"
        )
        return False

    if settings.TEST_TESTRUN_ID:
        if not settings.RUN_COUNT.isdigit():
            logger.error(f"RUN_COUNT provided is not an int: {settings.RUN_COUNT}")
            return False
        if int(settings.RUN_COUNT) > 10:
            logger.error(
                f"RUN_COUNT currently limited to 10 to conserve resources, {settings.RUN_COUNT} > 10"
            )
            return False
    return True


def check_launch_reproducer(settings):
    # Currently no specific requirements for this stage
    return True


def check_all_pipeline_stages(settings):
    check_download_prerequisites_ok = check_download_prerequisites(settings)
    if not check_download_prerequisites_ok:
        return False
    check_fetch_reproducer_ok = check_fetch_reproducer(settings)
    if not check_fetch_reproducer_ok:
        return False
    check_create_plan_ok = check_create_plan(settings)
    if not check_create_plan_ok:
        return False
    check_launch_reproducer_ok = check_launch_reproducer(settings)
    if not check_launch_reproducer_ok:
        return False
    return True
