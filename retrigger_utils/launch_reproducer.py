#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2024-present Linaro Limited
#
# SPDX-License-Identifier: MIT

import sys

sys.path.append(".")
from retrigger_utils import (  # noqa
    check_launch_reproducer,
    install_prerequisites,
    Settings,
    log_proc,
    run_cmd,
)


#
#   Required environment variables
#
required_vars = [
    # Token to submit to Tuxsuite
    "TUXSUITE_TOKEN",
    "QA_SERVER",
    "QA_TEAM",
    "QA_PROJECT",
    "SQUAD_BUILD_NAME",
    "JSON_OUT_NAME",
    "OUTPUT_REPRODUCER_FILE",
    "TUXSUITE_PUBLIC_KEY",
    "QA_REPORTS_TOKEN",
]


def launch_reproducer(settings):
    callback_parameter = f"{settings.QA_SERVER}/api/fetchjob/{settings.QA_TEAM}/{settings.QA_PROJECT}/{settings.SQUAD_BUILD_NAME}/env/tuxsuite.com"

    tuxsuite_cmd = [
        "tuxsuite",
        "plan",
        settings.OUTPUT_REPRODUCER_FILE,
        "--json-out",
        settings.JSON_OUT_NAME,
        "--callback",
        callback_parameter,
        "--no-wait",
    ]
    proc = run_cmd(cmd=tuxsuite_cmd)

    log_proc(proc, only_on_error=False)

    return proc.ok


def main():
    settings = Settings(extra=required_vars)
    if settings.missing:
        return False

    check_launch_reproducer_ok = check_launch_reproducer(settings)
    if not check_launch_reproducer_ok:
        return False

    install_prerequisites_ok = install_prerequisites(settings)
    if not install_prerequisites_ok:
        return False

    # Launch reproducer
    launch_reproducer_ok = launch_reproducer(settings=settings)
    return launch_reproducer_ok


if __name__ == "__main__":
    sys.exit(0 if main() else 1)
